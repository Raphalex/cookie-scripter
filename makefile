O_DIR = obj/
EXEC_DIR = bin/
SRC_DIR = src/
HEADERS_DIR = include/
CC = gcc
CFLAGS = -g -Wall
SOURCES = $(wildcard $(SRC_DIR)*.c)
OBJECTS = $(patsubst $(SRC_DIR)%.c, $(O_DIR)%.o, $(SOURCES))
EXEC = cookie_scripter
INIT_DIR = mkdir -p

#vpath %.c src
#vpath %.o obj
#vpath %.h include
#vpath cookie_scripter bin

$(EXEC_DIR)$(EXEC) : $(OBJECTS)
	$(INIT_DIR) $(EXEC_DIR)
	$(CC) $(CFLAGS) -o $@ $^

$(O_DIR)$(EXEC).o : $(SRC_DIR)$(EXEC).c
	$(INIT_DIR) $(O_DIR)
	$(CC) $(CFLAGS) -I $(HEADERS_DIR) -c  $< -o $@

$(O_DIR)%.o : $(SRC_DIR)%.c $(HEADERS_DIR)%.h
	$(INIT_DIR) $(O_DIR)
	$(CC) $(CFLAGS) -I $(HEADERS_DIR) -c $< -o $@

clean :
	rm $(OBJECTS) $(EXEC_DIR)$(EXEC)
